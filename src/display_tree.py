from anytree import RenderTree, Node
from anytree.exporter import DotExporter


def attach_children(parent_node, parent_anytree_node):
    for child_node in parent_node.get_children():
        child_anytree_node = Node(
            child_node.get_node_full_name(), parent=parent_anytree_node)
        attach_children(child_node, child_anytree_node)


def convert_tree_to_anytree(tree):
    anytree = Node("Root")
    attach_children(tree, anytree)
    return anytree


def display_tree(tree):
    antytree = convert_tree_to_anytree(tree)
    for pre, fill, node in RenderTree(antytree):
        print("%s%s" % (pre, node.name))


def display_tree_png(tree):
    antytree = convert_tree_to_anytree(tree)
    for pre, fill, node in RenderTree(antytree):
        print("%s%s" % (pre, node.name))
    DotExporter(antytree).to_picture('../data/dt.png')
