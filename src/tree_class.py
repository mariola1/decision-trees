class TreeNode():
    def __init__(self, node_name=None, node_value=None):
        self.children = []
        self.node_name = node_name
        self.node_value = node_value

    def add_child(self, child):
        self.children.append(child)

    def get_children(self):
        return self.children

    def get_node_name(self):
        return self.node_name

    def get_node_value(self):
        return self.node_value

    def is_root(self):
        return self.node_name is None and self.node_value is None

    def is_leaf(self):
        return len(self.children) == 0

    def get_node_full_name(self):
        if self.is_root():
            return '[Root]'
        return '[' + self.node_name + '=' + self.node_value + ']'
