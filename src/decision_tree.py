import src.decision_tree_functions as dt_fn
import src.display_tree as dt
import sys

if len(sys.argv) < 2:
    sys.exit('Please, input as arguments:\n' +
             '1. the name of the input CSV file.')

(heading, complete_data, incomplete_data, enquired_column) = dt_fn.file_to_order_csv_file(sys.argv[1])

tree = dt_fn.constuct_decision_tree(heading, complete_data, enquired_column)
dt.display_tree_png(tree)
