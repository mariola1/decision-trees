import csv
import math

from src.tree_class import TreeNode


def save_csv_file_as_a_list(csv_file):
    with open(csv_file, 'r') as f:
        csv_object = csv.reader(f)
        data_in_list_format = list(csv_object)
    return data_in_list_format


def is_complete(item, position):
    return item[position] != '?'


def order_csv_data(data_in_list_format):
    heading = data_in_list_format.pop(0)
    # We assume that in last coumn of csv file is enquired column
    completed_data = []
    incompleted_data = []
    enquired_column = len(heading) - 1
    [completed_data.append(item) if is_complete(item, enquired_column) \
         else incompleted_data.append(item) \
     for item in data_in_list_format]
    return (heading, completed_data, incompleted_data, enquired_column)


def file_to_order_csv_file(csv_file):
    data = save_csv_file_as_a_list(csv_file)
    ordered_data = order_csv_data(data)
    return ordered_data


def count_groups(completed_data, enquired_column):
    value_counts = {}
    for row in completed_data:
        if value_counts.get(row[enquired_column]) is None:
            value_counts[row[enquired_column]] = 0
        value_counts[row[enquired_column]] += 1
    return value_counts


def split_data_by_column(completed_data, column):
    value_counts = {}
    for row in completed_data:
        if value_counts.get(row[column]) is None:
            value_counts[row[column]] = []
        value_counts[row[column]].append(row)
    return value_counts


def entropy(completed_data, enquired_column):
    value_counts = count_groups(completed_data, enquired_column)
    entropy = 0
    for key, value in value_counts.items():
        probability = value / len(completed_data)
        entropy -= probability * math.log(probability, 2)
    return entropy


def calculate_information_gain(completed_data, column, enquired_column):
    information_gain = entropy(completed_data, enquired_column)
    data_groups = split_data_by_column(completed_data, column)
    for key, value in data_groups.items():
        information_gain -= len(value) / len(completed_data) * entropy(value, enquired_column)
    return information_gain


def list_of_available_column_indexes(heading, enquired_column):
    available_columns = []
    [available_columns.append(index) for index in range(len(heading)) if index != enquired_column]
    return available_columns


def decode_column_index_as_column_name(heading, available_columns):
    column_names = []
    [column_names.append(heading[index]) for index in available_columns]
    return column_names


def select_column(available_columns, completed_data, enquired_column):
    information_gain = -1
    selected_column = -1
    for column in available_columns:
        current_information_gain = calculate_information_gain(completed_data, column, enquired_column)
        if current_information_gain > information_gain:
            information_gain = current_information_gain
            selected_column = column
    return selected_column


def add_leaf(heading, completed_data, enquired_column, node):
    # all the remaining features have the same value so we can take first element from the list
    tc = TreeNode(heading[enquired_column], completed_data[0][enquired_column])
    leaf_node = tc
    node.add_child(leaf_node)


def delete_used_column(available_columns, selected_column):
    for i in range(len(available_columns)):
        if available_columns[i] == selected_column:
            available_columns.pop(i)
            break


def add_children(heading, available_columns, completed_data, enquired_column, node):
    if len(available_columns) == 0:
        add_leaf(heading, completed_data, enquired_column, node)
        return -1
    selected_column = select_column(available_columns, completed_data, enquired_column)
    delete_used_column(available_columns, selected_column)
    data_groups = split_data_by_column(completed_data, selected_column)
    if len(data_groups.items()) == 1:
        add_leaf(heading, data_groups[0], enquired_column)
        return -1
    for child_group, child_data in data_groups.items():
        tc = TreeNode(heading[selected_column], child_group)
        child = tc
        add_children(heading, list(available_columns), child_data, enquired_column, child)
        node.add_child(child)


def constuct_decision_tree(heading, completed_data, enquired_column):
    tc = TreeNode()
    available_columns = []
    [available_columns.append(column) for column in range(len(heading)) if column != enquired_column]
    tree = tc  # root
    add_children(heading, available_columns, completed_data, enquired_column, tree)
    return tree
