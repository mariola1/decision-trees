# Decision Trees

Decision Trees Algorithm -  belongs to the family of supervised learning algorithms. Decision trees classify the examples by sorting them down the tree from the root to some leaf/terminal node, with the leaf/terminal node providing the classification of the example.
Each node in the tree acts as a test case for some attribute, and each edge descending from the node corresponds to the possible answers to the test case. This process is recursive in nature and is repeated for every subtree rooted at the new node.
More about decision tree algorithm --> https://en.wikipedia.org/wiki/Decision_tree_learning

On Linux for correct work You should install `graphviz` library using below command:
```sudo apt install python-pydot python-pydot-ng graphviz```


**Decision Trees Algorithm Implementation**

1. Decision tree is constructed from data specified in a CSV file.

2. Format of a CSV file:

    *  Each data item is written on one line, with its variables separated by a comma.
    
    *  The last variable is used as a decision variable to branch a node and construct the decision tree.

3. In order to vizualize the decision tree constructed by the ID3 algorithm anytree module is used.
