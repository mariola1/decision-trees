from unittest import TestCase

from src.decision_tree_functions import is_complete


class Test(TestCase):
    def test_is_complete_with_item_contains_special_character(self):
        """
        Test that is_complete return False if contain ? character for given position
        """
        given_item = {'test': '?'}
        result = is_complete(given_item, 'test')
        self.assertFalse(result)

    def test_is_complete_with_item_not_contains_special_character(self):
        """
        Test that is_complete return True if not contain ? character for given position
        """
        given_item = {'test': '1'}
        result = is_complete(given_item, 'test')
        self.assertTrue(result)
